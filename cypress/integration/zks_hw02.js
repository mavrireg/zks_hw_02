describe('ToDo list items', function () {
    before(() => {
        cy.visit('/')
        cy.wait(200)
        cy.get('.todo-list li').should('have.length', 0)
        cy.screenshot()
    })

  beforeEach(() => {
        cy.wait(1000)
    })

    afterEach(() =>{
        cy.screenshot()
        cy.wait(500)
    })

    it('Enter Items to ToDo list', function () {


            cy.get('.new-todo')
            .type(`${'buy tickets'} {enter}`)
            .type(`${'do homework'} {enter}`)
            .type(`${'play the game'} {enter}`)
            .type(`${'call my friends'} {enter}`)
    })
    it('Select 4th element as completed', function () {

        cy.contains('.todo-list li', 'call my friends')
            .find('.toggle').check()
        cy.contains('.todo-list li', 'call my friends')
            .should('have.class', 'completed')
    })

    it('change some css properties and complete 3rd item', function () {

        cy.contains('.todo-list li', 'play the game')
            .find('.toggle').check()

        cy.get('body')
            .invoke('css', 'background-color', 'black')
            .should('have.css', 'background-color', 'rgb(0, 0, 0)')

        cy.get('h1')
            .invoke('css', 'color', 'rgb(0, 241, 63)')
            .should('have.css', 'color', 'rgb(0, 241, 63)')

        cy.get('.todoapp')
            .invoke('css', 'background-color', 'rgb(255, 187, 218)')
            .should('have.css', 'background-color', 'rgb(255, 187, 218)')

        cy.get('.todoapp')
            .invoke('css', 'font-family', 'monospace')
            .should('have.css', 'font-family', 'monospace')
            
        cy.screenshot()
        cy.wait(500)
        cy.get('.todo-list').toMatchImageSnapshot({
                    threshold: 0.001,
                })


    })
})
